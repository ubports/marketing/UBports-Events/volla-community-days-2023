# Volla Community Days 2023

UPDATE. It's that time again! We invite you to the fifth Volla Community Days from May 20 to 21. Meet the Volla team, partners and users in Remscheid or follow the latest developments and exciting outlooks live on the internet. Experience international guests in presentations, demos, interviews and new formats like one-to-one talks for companies and meet-one experts for users and developers.

STREAMING LINK: https://youtube.com/live/EiN3heE_7w8?feature=share


## Program

### Saturday, May 20, 2023

## Welcome reception and opening

10:00 - 10:40: Keynote - Hello World Systems, Dr. Jörg Wurzer, Managing Director: Volla. Factor 2. status and outlook
10:40 - 10:50: Hackathon - Presentation of challenges, prices and start
10:50 - 11:00: Questions and answers
11:00 - 11:20: Interview - Gigaset Communications (DE), Jörg Wissing, Senior Product & Business Development Manager: Manufacturing and Quality from Germany.
11:20 - 11:50: Presentation - UBports Foundation (DE), Alfred Neumeier, Volunteer Member of the Core Developer Team: Focal. A milestone. A roadmap.
11:50 - 12:00: Questions and answers.
12:00

12:00 - 12:10: Hello Volla - Live greeting from our resellers
12:10 - 12:40: Presentation - Linux Nordic (DK), Robin Haug, Managing Director - Alternatives to Big Tech for Scandinavia and beyond.
12:40

Lunch break - lunch, network, discuss, discover
14:00

## Volla in everyday life

14:00 - 14:30: Interview - RLMsolutions (US). Robb McColley, Managing Director - Go West. Volla for USA.
14:30 - 15:00: Practical Tips - Dr. Jörg Wurzer, Hallo Welt Systeme (DE): New functions. More comfort.
15:00 - 15:30: Impulse - StartMail, Alex van Eesteren - Secure e-mail communication and anonymous search on the Internet.
15:30 - 17:00: Networking - Parallel start of one-to-one conversations of merchants, companies and organizations.
15:30 - 16:00: Demo and presentation - Net.print Datensysteme, Stefan Baust, Head of Development. Mobile POS solution with Ubuntu Touch
16:00 - 16:30: Demo and interview - SAFETY.brands Germany, Felix Hänel, Member of the Management Board: 30 minutes that save lives. The Smartphone for the fire department.
16:30 - 17:00: Demo and presentation - GNUSolidaro (ES), Luis Falcon, Managing Director: Self-determined health. The app for patient data and emergency information.
17:00

Hello World Systems, Dr. Jörg Wurzer - Summary and conclusion of the first day

### Sunday, May 21, 2023

## Privacy, partners and perspectives

10:00 - 10:20: Interview - Hide.me (MY), Robert Šmorhaj, Partner Manager: No chance for surveillance and censorship.
10:20 - 10:35: Practical Tips - How to protect your data and communications on a Volla Phone.
10:35 - 10:40: Impulse - Trifact (CH): Convenience for patients. Volla Phone in the clinic
10:40 - 11:10: Presentation - umlaut SE (DE), Peter Seidenberg, Technical Managing Director: peedSMART OS. Fail-safe communication for rescue operations.
11:10 . 11:40: Demo - Cogia, Pascal Lauria, CEO - Cogia Phone for secure communication and file sharing with protection from prying eyes.
11:40 . 12:20: Impulse and Discussion - Hello World Systems, Dr. Jörg Wurzer, Holochain, N.N.. - A distributed and encrypted cloud as a paradigm shift for an independent and secure infrastructure of communication
12:20 - 12:30: Impulse - News from the world of community projects
12:30

Lunch Break - Lunch, Network, Discuss, Discover.
13:30

13:30 - 14:00: Demo - Erik Inkinen, Community Developer - Linux has never been so beautiful. Introduction to the Cutie Shell Community Project using Droidian on the Volla Phone as an example.
14:00 - 14:30: Presentation - Luka Panio, Volla Developer - Two worlds coming together. The convergence of cell phone and PC.
14:30 - 15:30: Hackathon - presentations and awarding of results.
15:30 Closing - Hallo Welt Systeme, Dr. Jörg Wurzer: Summary and outlook
